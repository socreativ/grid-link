<?php
    $id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
    $css = isset($block['className']) ? $block['className'] : '';
?>

<section id="<?= $id ?>" class="<?= $css ?>">

    <div class="container grid-section-container">
        <!-- 2 -->
        <?php if(!my_wp_is_mobile()): ?>

            <div class="seconde-space">
                <?php 
                $link = get_field('link_2');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="button position-relative d-flex " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                    
                        <div class="img-grid-section">
                            <?php 
                            $image = get_field('image_2');
                            if( !empty( $image ) ): ?>
                                <img class="anim-300" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="text-grid-container">
                            <h3>
                                <?php echo esc_html( $link_title ); ?>  
                            </h3>
                            <div class="wp-block-button is-style-light-link">
                                <span class="wp-block-button__link text-white">
                                    <?php _e('Read more','theme-socreativ'); ?>
                                </span>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <!-- 1 -->
        <div class="first-space">
            <?php 
            $link = get_field('link_1');
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="button position-relative d-flex " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                
                    <div class="img-grid-section">
                        <?php 
                        $image = get_field('image_1');
                        if( !empty( $image ) ): ?>
                            <img class="anim-300" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="text-grid-container">
                        <h3>
                            <?php echo esc_html( $link_title ); ?>  
                        </h3>
                        <div class="wp-block-button is-style-light-link">
                            <span class="wp-block-button__link text-white">
                                <?php _e('Read more','theme-socreativ'); ?>
                            </span>
                        </div>
                    </div>
                    <div class="icon-top-right-grid">
                        <?php if(get_field('label_top_right')){?>
                            <p><?php the_field('label_top_right'); ?></p>
                        <?php } ?>
                        <?php 
                        $icon = get_field('icon_top_right');
                        if( !empty( $icon ) ): ?>
                            <img class="anim-300" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>" />
                        <?php endif; ?>
                    </div>
                </a>
            <?php endif; ?>
        </div>
        <?php if(my_wp_is_mobile()): ?>
            <!-- 2 -->
            <div class="seconde-space">
                <?php 
                $link = get_field('link_2');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="button position-relative d-flex" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                    
                        <div class="img-grid-section">
                            <?php 
                            $image = get_field('image_2');
                            if( !empty( $image ) ): ?>
                                <img class="anim-300" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="text-grid-container">
                            <h3>
                                <?php echo esc_html( $link_title ); ?>  
                            </h3>
                            <div class="wp-block-button is-style-light-link">
                                <span class="wp-block-button__link text-white">
                                    <?php _e('Read more','theme-socreativ'); ?>
                                </span>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        <!-- 3 -->
        <div class="thir-space">
            <?php 
            $link = get_field('link_3');
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="button position-relative d-flex " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                
                    <div class="img-grid-section">
                        <?php 
                        $image = get_field('image_3');
                        if( !empty( $image ) ): ?>
                           <img class="anim-300" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="text-grid-container">
                        <h3>
                            <?php echo esc_html( $link_title ); ?>  
                        </h3>
                        <div class="wp-block-button is-style-light-link">
                            <span class="wp-block-button__link text-white">
                                <?php _e('Read more','theme-socreativ'); ?>
                            </span>
                        </div>
                    </div>
                </a>
            <?php endif; ?>
        </div>
        <!-- 4 -->
        <div class="fourth-space">
            <?php 
            $link = get_field('link_4');
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="button position-relative d-flex " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                
                    <div class="img-grid-section">
                        <?php 
                        $image = get_field('image_4');
                        if( !empty( $image ) ): ?>
                            <img class="anim-300" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="text-grid-container">
                        <h3>
                            <?php echo esc_html( $link_title ); ?>  
                        </h3>
                        <div class="wp-block-button is-style-light-link">
                            <span class="wp-block-button__link text-white">
                                <?php _e('Read more','theme-socreativ'); ?>
                            </span>
                        </div>
                    </div>
                </a>
            <?php endif; ?>
        </div>
    </div>
</section>